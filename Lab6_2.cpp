#include <iostream>
#include <ctime>

/*
��������� ��������� ��� �������� ������, 
��� ������� ��������������� ����� �������������� 
�������� �������� � ������������.
���������� � �����.
*/

const int N = 5;

struct T_List
{
	T_List* next;
	int age;

};

void ADD(T_List* head, int age)
{
	T_List* p = new T_List;
	p->age = age;

	p->next = head->next;
	head->next = p;
}

void PRINT(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		std::cout << p->age << std::endl;
		p = p->next;
	}
}

void DELETE_BY_CONDITION(T_List* head)
{
	T_List* tmp;
	T_List* p = head;
	while (p->next != nullptr)
	{
		if (p->next->age % 2 == 0)
		{
			tmp = p->next;
			p->next = p->next->next;
			delete tmp;
		}
		else
			p = p->next;
	}
}

void DUPLICATE_BY_CONDITION(T_List* head)
{
	T_List* p = head->next;
	while (p != nullptr)
	{
		if (p->age % 2 == 1)
		{
			T_List* q = new T_List;
			q->next = p->next;
			p->next = q;
			q->age = p->age;
			p = p->next;
		}
		p = p->next;
	}
}

int SEARCH(T_List* head, int key)//return index
{
	T_List* p = head;
	for (int i = 0; i < N; i++)
	{
		if (p->age == key)
		{
			return i;
		}
		p = p->next;
	}
	return -1; // not found

}

void CLEAR(T_List* head)
{
	T_List* tmp;
	T_List* p = head->next;
	while (p != nullptr)
	{
		tmp = p;
		p = p->next;
		delete tmp;
	}
}

int main()
{
	T_List* head = new T_List;
	head->next = nullptr;

	//���������� ������ ���������� �������
	//srand(time(0));
	int i = 0;
	int arr[N];
	while (i < N)
	{
		int x = rand() % 10 + 1;
		ADD(head, x);

		for (int l = 0; l < N; l++)
			arr[l] = x;

		i++;
	}
	PRINT(head);
	std::cout << "-----" << std::endl;
	
	DELETE_BY_CONDITION(head);

	PRINT(head);
	std::cout << "-----" << std::endl;

	int k = 5;
	std::cout << "Element: " << k << " Index: " << SEARCH(head, k) << std::endl;
	std::cout << "-----" << std::endl;

	DUPLICATE_BY_CONDITION(head);

	PRINT(head);
	std::cout << "-----" << std::endl;
	
	CLEAR(head);
	delete head;
	return 0;
}